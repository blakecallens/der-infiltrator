CC65 = cc65
CA65 = ca65
LD65 = ld65
INPUT = main
OUTPUT = game
EMULATOR = fceux


$(OUTPUT).nes: $(INPUT).o reset.o asm4c.o nes.cfg
	$(LD65) -o dist/$(OUTPUT).nes -C nes.cfg reset.o $(INPUT).o asm4c.o nes.lib
	rm *.o
	rm $(INPUT).s
	@echo $(OUTPUT).nes created

reset.o: reset.s
	$(CA65) reset.s

asm4c.o: asm4c.s
	$(CA65) asm4c.s

$(INPUT).o: $(INPUT).s
	$(CA65) $(INPUT).s

$(INPUT).s: $(INPUT).c
	$(CC65) -Oi $(INPUT).c --add-source