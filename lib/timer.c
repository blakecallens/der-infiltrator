#pragma bss-name(push, "ZEROPAGE")
unsigned char frames1;
unsigned char frames2;
unsigned char seconds1;
unsigned char seconds2;
unsigned char minutes1;
unsigned char minutes2;

void timer_tick(void) {
  /*++frames;
  if(frames > 59) {
    ++seconds;
    frames = 0;
  }
  if(seconds > 59) {
    ++minutes;
    seconds = 0;
  }*/

  ++frames1;
  if(frames1 > 9) {
    ++frames2;
    frames1 = 0;
  }
  if(frames2 > 5) {
    ++seconds1;
    frames2 = 0;
  }
  if(seconds1 > 9) {
    ++seconds2;
    seconds1 = 0;
  }
  if(seconds2 > 5) {
    ++minutes1;
    seconds2 = 0;
  }
  if(minutes1 > 9) {
    ++minutes2;
    minutes1 = 0;
  }
  if(minutes2 > 9) {
    minutes2 = 0;
  }
}

void draw_time(void) {
  PPU_ADDRESS = 0x20;  	//	set an address in the PPU of 0x21ca
  PPU_ADDRESS = 0x41;
  
  PPU_DATA = minutes2 + 0x30;
  PPU_DATA = minutes1 + 0x30;
  PPU_DATA = 0x3A;
  PPU_DATA = seconds2 + 0x30;
  PPU_DATA = seconds1 + 0x30;
  PPU_DATA = 0x3A;
  PPU_DATA = frames2 + 0x30;
  PPU_DATA = frames1 + 0x30;
}