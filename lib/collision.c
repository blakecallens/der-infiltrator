/**
 * Builds a collision rect based on a tile position definition
 */
void get_tile_rect(unsigned char tile_def[2], unsigned char tile_rect[4]) {
  unsigned char temp_pos = tile_def[0];

  tile_rect[0] = tile_def[1] * 0x08;
  tile_rect[1] = 0x00;
  tile_rect[2] = 0x08;
  tile_rect[3] = 0x08;

  while(temp_pos >= 0x10) {
    tile_rect[1] += 0x08;
    temp_pos -= 0x10;
  }
  while(temp_pos > 0x00) {
    tile_rect[0] += 0x10;
    temp_pos -= 0x01;
  }
}

void get_hero_rect(unsigned char hero_x_pos, unsigned char hero_y_pos, unsigned char hero_rect[4]) {
  hero_rect[0] = hero_x_pos + 0x01,
  hero_rect[1] = hero_y_pos >= 0x80 ? hero_y_pos - 0x80 : hero_y_pos,
  hero_rect[2] = 0x06;
  hero_rect[3] = 0x08;
}

void get_collision_tile_def(unsigned char sprite_rect[4], unsigned char tile_direction, unsigned char tile_def[2]) {
  unsigned char temp_x_pos = sprite_rect[0];
  unsigned char temp_y_pos = sprite_rect[1];
  tile_def[0] = 0x00;

  while(temp_y_pos >= 0x08) {
    tile_def[0] += 0x10;
    temp_y_pos -= 0x08;
  }
  while(temp_x_pos >= 0x10) {
    tile_def[0] += 0x01;
    temp_x_pos -= 0x10;
  }

  tile_def[1] = temp_x_pos >= 0x04 ? 0x01 : 0x00;

  switch(tile_direction) {
    case 0x00:
      tile_def[0] -= 0x10;
      break;
    case 0x02:
      tile_def[0] += 0x10;
      break;
    default:
      break;
  }
}

const unsigned char is_colliding(unsigned char item_1[4], unsigned char item_2[4]) {
  if(
    item_1[0] < item_2[0] + item_2[2] &&
    item_1[0] + item_1[2] > item_2[0] &&
    item_1[1] < item_2[1] + item_2[3] &&
    item_1[1] + item_1[3] > item_2[1]
  ) {
    return 0x01;
  }

  return 0x00;
}