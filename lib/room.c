#include "rooms/00.h"

unsigned char current_room = 0x00;
const unsigned char * const * const rooms[] = {room_00};

void goto_room(unsigned char room) {
  current_room = room;
}

void get_room_tiles(unsigned char slot, unsigned char * outTiles) {
  outTiles[0] = 0x00;
  outTiles[1] = slot;

  while(outTiles[1] >= 0x10) {
    outTiles[1] -= 0x10;
    outTiles[0] += 0x01;
  }
}

void draw_room(void) {
  unsigned char tiles[2];
  PPU_ADDRESS = 0x20;
  PPU_ADDRESS = 0xA0;

  for(i = 0x00; i < 0xC0; ++i) {
    get_room_tiles(rooms[current_room][2][i], tiles);
    PPU_DATA = rooms[current_room][1][tiles[0]];
    PPU_DATA = rooms[current_room][1][tiles[1]];
  }

  PPU_ADDRESS = 0x22;
  PPU_ADDRESS = 0x20;

  for(i = 0x00; i < 0xC0; ++i) {
    get_room_tiles(rooms[current_room][3][i], tiles);
    PPU_DATA = rooms[current_room][1][tiles[0]];
    PPU_DATA = rooms[current_room][1][tiles[1]];
  }
}