enum {
  STAND_RIGHT,
  STAND_LEFT,
  RUN_RIGHT,
  RUN_LEFT,
  CLIMB_UP,
  CLIMB_DOWN,
  CROUCH,
  SLIDE,
};

enum {
  GROUND,
  JUMP,
};

const unsigned char ANIMATION_HERO_RUN[] = { //tile numbers
	0x00, 0x01, 0x02, 0x03, 0x04, 0x03, 0x02, 0x01
};

unsigned char hero_state[] = {
  STAND_RIGHT, // move state
  GROUND,      // jump state
  0x20,        // x position
  0xD7,        // y position
  0xA0,        // x speed
  0xA0,        // y speed
  0x00         // animation frame
};

unsigned char a_pressed = 0x00;
unsigned char b_pressed = 0x00;

void update_hero_speed (unsigned char right, unsigned char left, unsigned char up, unsigned char down) {
  hero_state[4] += right;
  hero_state[4] -= left;
  hero_state[5] -= up;
  hero_state[5] += down;

  if(hero_state[4] > 0xBC) {
    hero_state[4] = 0xBC;
  } else if (hero_state[4] < 0x85) {
    hero_state[4] = 0x85;
  }

  if(!right && !left) {
    if (hero_state[4] > 0xA0) {
      hero_state[4] -= hero_state[1] == GROUND ? 0x02 : 0x01;
    } else if (hero_state[4] < 0xA0) {
      hero_state[4] += hero_state[1] == GROUND ? 0x02 : 0x01;
    }
  }
  if(hero_state[1] != GROUND) {
    hero_state[5] += 0x03;
  }

  if(hero_state[5] > 0xC0) {
    hero_state[5] = 0xC0;
  } else if (hero_state[5] < 0x70) {
    hero_state[5] = 0x70;
  }
  
  if(hero_state[4] >= 0xA0) {
    hero_state[2] += (hero_state[4] - 0xA0) / 0x10;
  } else {
    hero_state[2] -= (0xA0 - hero_state[4]) / 0x10;
  }
  if(hero_state[5] >= 0xA0) {
    hero_state[3] += (hero_state[5] - 0xA0) / 0x10;
  } else {
    hero_state[3] -= (0xA0 - hero_state[5]) / 0x10;
  }
}

void update_hero_sprite (void) {
  unsigned char sprite_index = 0x00;
	SPRITES[sprite_index] = hero_state[3];
  ++sprite_index;
  SPRITES[sprite_index] = (hero_state[0] == RUN_RIGHT || hero_state[0] == RUN_LEFT) ? ANIMATION_HERO_RUN[hero_state[6]] : (hero_state[4] < 0xA8 && hero_state[4] > 0x99) ? 0x05 : 0x04;
  ++sprite_index;
	SPRITES[sprite_index] = (hero_state[0] == RUN_RIGHT || hero_state[0] == STAND_RIGHT) ? MetaSprite_Attrib[0] : MetaSprite_Attrib[1];
	++sprite_index;
  SPRITES[sprite_index] = hero_state[2];
  
  if (Frame_Count % 2 == 0) {
		++hero_state[6];
		if (hero_state[6] >= sizeof(ANIMATION_HERO_RUN)) {
			hero_state[6] = 0x00;
		}
	}
}

void check_hero_collision(void) {
  unsigned char hero_rect[4];
  unsigned char h_tile_rect[4];
  unsigned char v_tile_rect[4];
  unsigned char tile_def[2];
  unsigned char tiles[2];
  unsigned char tile_group = hero_state[3] >= 0x80 ? 0x03 : 0x02;

  get_hero_rect(hero_state[2], hero_state[3], hero_rect);
  get_collision_tile_def(hero_rect, hero_state[4] >= 0xA0 ? 0x01 : 0x03, tile_def);
  get_room_tiles(rooms[current_room][tile_group][tile_def[0]], tiles);

  if(tiles[tile_def[1]] > 0x00) {
    get_tile_rect(tile_def, h_tile_rect);
  }

  if(is_colliding(hero_rect, h_tile_rect)) {
    hero_state[4] = 0xA0;
    switch(hero_state[0]) {
      case RUN_RIGHT:
      case STAND_RIGHT:
        hero_state[2] = h_tile_rect[0] - 0x07;
        break;
      case RUN_LEFT:
      case STAND_LEFT:
        hero_state[2] = h_tile_rect[0] + 0x07;
        break;
      default:
        break;
    }
  }

  if(hero_state[5] >= 0xA0) {
    get_collision_tile_def(hero_rect, 0x02, tile_def);
    get_room_tiles(rooms[current_room][tile_group][tile_def[0]], tiles);

    if(tiles[tile_def[1]] > 0x00) {
      get_tile_rect(tile_def, v_tile_rect);

      if(is_colliding(hero_rect, v_tile_rect)) {
        if(hero_state[1] == JUMP) {
          hero_state[5] = 0xA0;
          hero_state[1] = GROUND;
          hero_state[3] = v_tile_rect[1] - 0x01 + (tile_group == 0x03 ? 0x80 : 0x20);
        }
      }
    } else if(hero_state[1] == GROUND) {
      hero_state[1] = JUMP;
    }
  }/* else if (hero_state[1] == JUMP) {
    get_collision_tile_def(hero_rect, 0x00, tile_def);
    get_room_tiles(rooms[current_room][tile_group][tile_def[0]], tiles);

    if(tiles[tile_def[1]] > 0x00) {
      get_tile_rect(tile_def, tile_rect);
    }

    if(is_colliding(hero_rect, tile_rect)) {
      hero_state[5] = 0xA0;
      hero_state[3] = tile_rect[1] + 0x10 + (tile_group == 0x03 ? 0x80 : 0x20);
    }
  }*/

  /*minutes2 = 0x00;
  while(hero_rect[0] >= 0x10) {
    minutes2 += 0x01;
    hero_rect[0] -= 0x10;
  }
  if(minutes2 > 0x09) {
    minutes2 += 0x30 - 0x09;
  }
  minutes1 = hero_rect[0];
  if(minutes1 > 0x09) {
    minutes1 += 0x30 - 0x09;
  }*/

  seconds2 = 0x00;
  while(tile_def[0] >= 0x10) {
    seconds2 += 0x01;
    tile_def[0] -= 0x10;
  }
  if(seconds2 > 0x09) {
    seconds2 += 0x30 - 0x09;
  }
  seconds1 = tile_def[0];
  if(seconds1 > 0x09) {
    seconds1 += 0x30 - 0x09;
  }
}

void update_hero (void) {
  if ((joypad1 & RIGHT) != 0) {
    hero_state[0] = RUN_RIGHT;
		update_hero_speed(hero_state[1] == GROUND ? 0x02 : 0x01, 0x00, 0x00, 0x00);
	} else if ((joypad1 & LEFT) != 0) {
    hero_state[0] = RUN_LEFT;
		update_hero_speed(0x00, hero_state[1] == GROUND ? 0x02 : 0x01, 0x00, 0x00);
	} else {
    hero_state[0] = (hero_state[0] == RUN_RIGHT || hero_state[0] == STAND_RIGHT) ? STAND_RIGHT : STAND_LEFT;
		update_hero_speed(0x00, 0x00, 0x00, 0x00);
  }

  if ((joypad1 & A_BUTTON) !=0) {
    if(hero_state[1] == GROUND && !a_pressed) {
      hero_state[1] = JUMP;
      a_pressed = 0x01;
      update_hero_speed(0x00, 0x00, 0x40, 0x00);
    }
  } else if (hero_state[1] == GROUND) {
    a_pressed = 0x00;
  }
  
  check_hero_collision();
  update_hero_sprite();
}