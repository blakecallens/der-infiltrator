#include "lib/definitions.c"
#include "lib/timer.c"
#include "lib/room.c"
#include "lib/collision.c"
#include "lib/hero.c"

void main (void) {
	All_Off();	// turn off screen
	X1 = 0x77;	// starting position of the top left sprite
	Y1 = 0x77;	// near the middle of screen
	Load_Palette();

	draw_room();

	All_On();	// turn on screen

	while (1){	// infinite loop
		while (NMI_flag == 0);	// wait till NMI

		//timer_tick();
		draw_time();
	
		Reset_Scroll();
		
		Get_Input();
		//move_logic();
		//update_Sprites();
		update_hero();
		
		NMI_flag = 0;
	}
}
	
// inside the startup code, the NMI routine will ++NMI_flag and ++Frame_Count at each V-blank
	
void All_Off (void) {
	PPU_CTRL = 0;
	PPU_MASK = 0; 
}

	
void All_On (void) {
	PPU_CTRL = 0x90; // screen is on, NMI on
	PPU_MASK = 0x1e; 
}

	
void Reset_Scroll (void) {
	PPU_ADDRESS = 0;
	PPU_ADDRESS = 0;
	SCROLL = 0;
	SCROLL = 0;
}

	
void Load_Palette (void) {
	PPU_ADDRESS = 0x3f;
	PPU_ADDRESS = 0x00;
	for( index = 0; index < sizeof(PALETTE); ++index ){
		PPU_DATA = PALETTE[index];
	}
}